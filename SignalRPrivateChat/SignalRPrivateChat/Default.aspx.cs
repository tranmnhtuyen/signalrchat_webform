﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SignalRPrivateChat
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {

            char[] MangKyTu = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };

            //tạo một chuỗi ngẫu nhiên
            Random fr = new Random();
            string chuoi = "";
            for (int i = 0; i < 6; i++)
            {
                int t = fr.Next(0, MangKyTu.Length);
                chuoi = chuoi + MangKyTu[t];
            }
            txtId.Text = chuoi.ToString();
            if (txtTen.Text == "admin")
            {
                Session["UserName"] = txtTen.Text;
                Session["UserId"] = 0;
                Response.Redirect("~/StartChat.aspx");
            }
            else
            {
                Session["UserName"] = txtTen.Text;
                Session["UserId"] = txtId.Text;
                Response.Redirect("~/Home.aspx");
            }
           
        }
    }
}
﻿
var chatHub = $.connection.chatHub;
$(document).ready(function () {
    $('<audio id="chatAudio"><source src="' + srp + 'images/notify.ogg" type="audio/ogg"><source src="' + srp + 'images/notify.mp3" type="audio/mpeg"><source src="' + srp + 'images/notify.wav" type="audio/wav"></audio>').appendTo('body');
    // Declare a proxy to reference the hub. 
    var chatHub = $.connection.chatHub;
    registerClientMethods(chatHub);
    // Start Hub
    $.connection.hub.start().done(function () {
        registerEvents(chatHub);
    });

    $("#chat_min_button").click(function () {
        if ($(this).html() == "<i class=\"fa fa-minus-square\"></i>") {
            $(this).html("<i class=\"fa fa-plus-square\"></i>");
        }
        else {
            $(this).html("<i class=\"fa fa-minus-square\"></i>");
        }
        $("#chat_box").slideToggle();
    });

    setInterval(ResetTypingFlag, 6000);
});


function registerEvents(chatHub) {
    var UserName = $("[id$=hdnCurrentUserName]").val();
    var UserID = parseInt($("[id$=hdnCurrentUserID]").val());
    chatHub.server.connect(UserName, UserID);
   // console.log("registerEvents" + UserName + "," + UserID);
}

function registerClientMethods(chatHub) {
    // Calls when user successfully logged in
    chatHub.client.onConnected = function (id, userName, allUsers, messages, userid) {
        $('#hdId').val(id);
        $('#hdUserName').val(userName);

        // Add All Users
        for (i = 0; i < allUsers.length; i++) {
            //AddUser(chatHub, allUsers[i].ConnectionId, allUsers[i].UserName, userid);
            AddUser(chatHub, allUsers[i].UserID, allUsers[i].UserName, userid);
        }

        // Add Existing Messages
        for (i = 0; i < messages.length; i++) {//Gửi đi những tin nhắn có nội dung lớn hơn ""
            AddMessage(messages[i].UserName, messages[i].Message);
        }

    }
}

// On New User Connected

chatHub.client.onNewUserConnected = function (id, name, userid) {
    AddUser(chatHub, id, name, userid);
   
}

// On User Disconnected
chatHub.client.onUserDisconnected = function (id, userName) {
    $('#' + id).remove();

    //var ctrId = 'private_' + id;
    //$('#' + ctrId).remove();
}

chatHub.client.messageReceived = function (userName, message) {
    AddMessage(userName, message);
}


chatHub.client.sendPrivateMessage = function (windowId, fromUserName, chatTitle, message) {
    //console.log("tin nhan:" + message);
    var ctrId = 'private_' + windowId;
    if ($('#' + ctrId).length == 0) {
        createPrivateChatWindow(chatHub, windowId, ctrId, fromUserName, chatTitle);
        $('#chatAudio')[0].play();
    }
    else {
        var rType = CheckHiddenWindow();
        if ($('#' + ctrId).parent().css('display') == "none") {
            $('#' + ctrId).parent().parent().effect("shake", { times: 2 }, 1000);
            rType = true;
        }
        if (rType == true) {
            $('#chatAudio')[0].play();
        }
    }
    $('#' + ctrId).chatbox("option", "boxManager").addMsg(fromUserName, message);
    $('#typing_' + windowId).hide();
}
//Nội dung chat last
chatHub.client.GetLastMessages = function (TouserID, CurrentChatMessages) {
    var ctrId = 'private_' + TouserID;
    var AllmsgHtml = "";
    for (i = 0; i < CurrentChatMessages.length; i++) {
        AllmsgHtml += "<div style=\"display: block; max-width: 200px;\" class=\"ui-chatbox-msg\">";
        if (i == CurrentChatMessages.length - 1) {
            if ($('#' + ctrId).children().last().html() != "<b>" + CurrentChatMessages[i].FromUserName + ": </b><span>" + CurrentChatMessages[i].Message + "</span>") {
                AllmsgHtml += "<b>" + CurrentChatMessages[i].FromUserName + ": </b><span>" + CurrentChatMessages[i].Message + "</span>";
            }
        }
        else {
            AllmsgHtml += "<b>" + CurrentChatMessages[i].FromUserName + ": </b><span>" + CurrentChatMessages[i].Message + "</span>";
        }
        AllmsgHtml += "</div>";
    }
    $('#' + ctrId).prepend(AllmsgHtml);
}

function CheckHiddenWindow() {
    var hidden, state;

    if (typeof document.hidden !== "undefined") {
        state = "visibilityState";
    } else if (typeof document.mozHidden !== "undefined") {
        state = "mozVisibilityState";
    } else if (typeof document.msHidden !== "undefined") {
        state = "msVisibilityState";
    } else if (typeof document.webkitHidden !== "undefined") {
        state = "webkitVisibilityState";
    }

    if (document[state] == "hidden")
        return true;
    else
        return false;

}
//Hiện List danh sác user online
function AddUser(chatHub, id, name, userid) { 
    var currentuserid = parseInt($("[id$=hdnCurrentUserID]").val());
    var connectionid = $('#hdId').val();

    var code = "";
    var code2 = "";
    if (connectionid == "") {
        if (userid == currentuserid) {
            $('#hdId').val(id);
            connectionid = id;
            
            $('#hdUserName').val(name);
        }
    } 
   
    //Người dùng đang online trong hệ thống
    if (connectionid != id) {
        if ($('#' + id).length == 0) {
            console.log("Id:" + id);
            console.log("Name:" + name);
            code2 = $('<div class="recycle-chat" id="' + id + '" ><div class="icon_chat2"><span>' + name + '</span></div</div>');
            code = $('<a class="icon_chat" id="' + id + '" class="col-sm-12 bg-success" ><span class="chat_icon_name">' + name + '</span><a>');

            $(code).click(function () {
                var id = $(this).attr('id');
                if (connectionid != id) {
                    var icon = document.getElementById('testnha');//Ẩn icon chat khi mở hộp thoại chat
                    icon.style.display = 'none';
                    OpenPrivateChatWindow(chatHub, id, name);
                }
            })
            $(code2).click(function () {
                var id = $(this).attr('id');
                console.log("dasdasdad" + id);
                if (connectionid != id) {
                    OpenPrivateChatWindow(chatHub, id, name);
                }
            });
        }
    }
    else {// Info người dùng đang login
       
           // code = $('<div id="curruser_' + id + '" class="col-sm-12 bg-info"  ><i class=\"fa fa-user\"></i> ' + name + '<div>');
    }
    if (id == 0) {
        $("#testnha").append(code);//Hiện danh icon để chat với Admin
    }
    if (connectionid == 0) {
        $("#side_chat").append(code2);
    }
   
}

//Mở cửa sổ chat
function OpenPrivateChatWindow(chatHub, id, userName) {
    var ctrId = 'private_' + id;
    if ($('#' + ctrId).length > 0) return;
    createPrivateChatWindow(chatHub, id, ctrId, userName, userName);

   
}
//Hiện cửa sổ chat
function createPrivateChatWindow(chatHub, userId, ctrId, userName, chatTitle) {
    //console.log("userName" + userName)
    //console.log("showlist:" + showList.length)
    $("#chat_div").append("<div id=\"" + ctrId + "\"></div>")
    showList.push(ctrId);


    //code = $('<a id="' + userId + '" class="col-sm-12 bg-success" > <i class=\"fa fa-user\"></i> ' + userName + '<a>');
    //$("#side_chat").append(code);//hiện danh sách người dùng vào trang Admin khi User bắt đầu chat
    //var connectionid = $('#hdId').val();
    //$(code).click(function () {
    //    var id = $(this).attr('id');
    //    console.log("asfdsadas:" + id);
    //    if (connectionid != id) {
    //        OpenPrivateChatWindow(chatHub, id, userName);
    //    }
    //});
    if (ctrId != "private_0") {// là hiện trong trang ADmin
        $('#' + ctrId).chatbox({
            id: ctrId,
            title: chatTitle.slice(0, 5),
            user: userName,
            offset: getNextOffset2(),
            width: 250,
            messageSent: function (id, user, msg) {
                chatHub.server.sendPrivateMessage(userId, msg);
                TypingFlag = true;
            },
            boxClosed: function (removeid) {
                $('#' + removeid).remove();
                var icon = document.getElementById('testnha');// Hiện lại icon chat khi tắt hộp thoại chat
                icon.style.display = 'block';
                var idx = showList.indexOf(removeid);
                if (idx != -1) {
                    showList.splice(idx, 1);
                    diff = config.width + config.gap;
                    for (var i = idx; i < showList.length; i++) {
                        offset = $("#" + showList[i]).chatbox("option", "offset");
                        $("#" + showList[i]).chatbox("option", "offset", offset - diff);
                    }
                }
            }

        });

    } else {
        $('#' + ctrId).chatbox({
            id: ctrId,
            title: chatTitle.slice(0, 5),
            user: userName,
            offset: getNextOffset(),
            width: 250,
            messageSent: function (id, user, msg) {
                chatHub.server.sendPrivateMessage(userId, msg);
                TypingFlag = true;
            },
            boxClosed: function (removeid) {
                $('#' + removeid).remove();
                var icon = document.getElementById('testnha');// Hiện lại icon chat khi tắt hộp thoại chat
                icon.style.display = 'block';
                var idx = showList.indexOf(removeid);
                if (idx != -1) {
                    showList.splice(idx, 1);
                    diff = config.width + config.gap;
                    for (var i = idx; i < showList.length; i++) {
                        offset = $("#" + showList[i]).chatbox("option", "offset");
                        $("#" + showList[i]).chatbox("option", "offset", offset - diff);
                    }
                }
            }

        });

    }
    $('#' + ctrId).siblings().css("position", "relative");
    $('#' + ctrId).siblings().append("<div id=\"typing_" + userId + "\" style=\"width:20px; height:20px; display:none; position:absolute; right:14px; top:8px\"></div>");
    $('#' + ctrId).siblings().find('textarea').on('input', function (e) {
        if (TypingFlag == true) {
            chatHub.server.sendUserTypingRequest(userId);
        }
        TypingFlag = false;
    });

    var FromUserID = parseInt($("[id$=hdnCurrentUserID]").val());
    var ToUserID = userId;
    chatHub.server.requestLastMessage(FromUserID, ToUserID);
}

chatHub.client.ReceiveTypingRequest = function (userId) {
    var ctrId = 'private_' + userId;
    if ($('#' + ctrId).length > 0) {
        jQuery('#typing_' + userId).show();
        jQuery('#typing_' + userId).delay(6000).fadeOut("slow");
    }
}

// list of boxes shown on the page
var showList = new Array();

var config = {//Thuộc tính CSS right = 20px 
    width: 250, //px
    gap:20,
    maxBoxes: 5
};
var getNextOffset = function () {  //hiện hội thoại kế tiếp trong Chat Admin
    return (20 ) * showList.length;
};

var getNextOffset2 = function () {
    if (showList.length == 1) {
       //console.log("bằng 1:" + showList.length);
        return (0);
    }
    else {
        //console.log("khác 1:" + showList.length);
        return (config.width + config.gap) * (showList.length-1) ;
    }
};

var TypingFlag = true;

function ResetTypingFlag() {
    TypingFlag = true;
}

function AddMessage(userName, message) {
    //$('#divChatWindow').append('<div class="message"><span class="userName">' + userName + '</span>: ' + message + '</div>');
    //var height = $('#divChatWindow')[0].scrollHeight;
    //$('#divChatWindow').scrollTop(height);
}